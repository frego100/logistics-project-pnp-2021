<?php

namespace App\Exceptions;

use Exception;

class InternalServerError extends Exception { 
    const DESCRIPTION = "Internal Server Error (500)";
    
    public function __construct($details) {
        parent::__construct(self::DESCRIPTION.". ".$details);
    }
}