<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Throwable;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
     
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof BadRequestException) {
            $data = new ErrorMessage($exception, $request->path());
            return response()->json($data, 400);
        }
        
        if ($exception instanceof InternalServerError) {
            $data = new ErrorMessage($exception, $request->path());
            return response()->json($data, 500);
        }

        if ($exception instanceof ValidationException) {
            $response = $this->errorMessagesValidation($exception->errors(), $request->path());
            return response()->json($response, 400);
        }
        if ($exception instanceof ModelNotFoundException) {
            $response = $this->errorNotFoundModel($exception->getModel(), $request->path());
            return response()->json($response, 404);
        }
        
        //Authorization Token not found
        $data = new ErrorMessage($exception, $request->path());
        return response()->json($data, 500);;
    }

    private function errorMessagesValidation($errors, $path)
    {
        return [
            'path' => $path,
            'message' => BadRequestException::DESCRIPTION . "Validation Error.",
            'data' => $errors
        ];
    }
    // donde $model tiene la siguiente estructura App/Models/{Model}
    private function errorNotFoundModel($model, $path)
    {
        $model = $this->onlyGetModel($model);
        return [
            'path' => $path,
            'message' => $model . " not found",
        ];
    }

    private function onlyGetModel($model)
    {
        $array = explode('\\', $model);
        $onlyModel = end($array);
        return $onlyModel;
    }
}
