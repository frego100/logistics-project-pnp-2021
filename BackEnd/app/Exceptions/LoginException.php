<?php

namespace App\Exceptions;


class LoginException extends BadRequestException{ 
    
    public function __construct($details) {
        parent::__construct($details);
    }
}