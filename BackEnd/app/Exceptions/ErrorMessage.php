<?php

namespace App\Exceptions;

use Throwable;

class ErrorMessage { 
    public $path;
    public $message;
    public $exception;

    public function __construct(Throwable $exception, $path) {
       $this->exception = get_class($exception); 
       $this->message = $exception->getMessage();
       $this->path = $path;
    }
}