<?php

namespace App\Exceptions;

use Exception;

class BadRequestException extends Exception { 
    const DESCRIPTION = "Bad Request (400)";
    
    public function __construct($details) {
        parent::__construct(self::DESCRIPTION.". ".$details);
    }
}