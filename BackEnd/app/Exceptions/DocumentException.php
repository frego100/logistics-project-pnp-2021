<?php

namespace App\Exceptions;


class DocumentException extends InternalServerError { 
    const DESCRIPTION = "Document. File exception";
    
    public function __construct($details) {
        parent::__construct(self::DESCRIPTION.". ".$details);
    }
}