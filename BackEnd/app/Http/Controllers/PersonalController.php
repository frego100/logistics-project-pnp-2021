<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalRequest;
use App\Http\Services\PersonalService;
use App\Models\Personal;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    private $service;
    public function __construct(PersonalService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(PersonalRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(PersonalRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}



