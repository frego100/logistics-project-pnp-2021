<?php

namespace App\Http\Controllers;

use App\Http\Services\TipoFormatoService;
use App\Http\Requests\TipoFormatoStoreRequest;
use Illuminate\Http\Request;

class TipoFormatoController extends Controller
{
    private $service;
    public function __construct(TipoFormatoService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(TipoFormatoStoreRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(TipoFormatoStoreRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}
