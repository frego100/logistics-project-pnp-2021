<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Services\LoginService;
use Illuminate\Http\Request;

/**
 * @group Ubicaciones
 */
class LoginController extends Controller
{
    private $service;
    public function __construct(LoginService $service)
    {
        $this->service = $service;
    }   

    public function login(LoginRequest $request)
    {   
        $data = $this->service->login($request);
        return $this->response($data);
    }
}
