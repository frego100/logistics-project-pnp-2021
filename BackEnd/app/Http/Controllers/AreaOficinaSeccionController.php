<?php

namespace App\Http\Controllers;

use App\Http\Requests\AreaOficinaSeccionStoreRequest;
use App\Http\Services\AreaOficinaSeccionService;
use App\Http\Services\BienService;
use App\Models\AreaOficinaSeccion;
use Illuminate\Http\Request;

class AreaOficinaSeccionController extends Controller
{
    private $service;
    public function __construct(AreaOficinaSeccionService $service)
    {   
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(AreaOficinaSeccionStoreRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(AreaOficinaSeccionStoreRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}
