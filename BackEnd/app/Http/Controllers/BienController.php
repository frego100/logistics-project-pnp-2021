<?php

namespace App\Http\Controllers;

use App\Http\Services\BienService;
use Illuminate\Http\Request;
use App\Models\Bien;
use App\Models\TipoFormato;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;

class BienController extends Controller
{
    private $service;
    public function __construct(BienService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(Request $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(Request $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

    public function getAllBienes()
    {
        $bienes = Bien::all(); 
        return view('bienes', compact ('bienes'));
    }

    public function downloadPDF()
    {
        $bienes = Bien::all();
        $pdf = PDF::loadView('bienes', compact('bienes'));
        return $pdf->download('bienes '.date(DATE_RFC2822).'.pdf');
    }
    public function qr()
    {

        $productos = TipoFormato::orderBy('id','DESC')->get();

      

        return view('qr', compact('productos'));
   
    }
    public function qrGenerate(){

        QrCode::size(200)->format('svg')->margin(3)->backgroundColor(255,255,204)->generate('Codigo QR
        ','../public/qrcodes/qrcode.svg');
          
       }
    
}
