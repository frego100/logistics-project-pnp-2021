<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubunidadStoreRequest;
use App\Http\Services\SubunidadService;
use Illuminate\Http\Request;

class SubunidadController extends Controller
{
    private $service;
    public function __construct(SubunidadService $service)
    {
        $this->service = $service;
    }   
    public function index()
    {
        return $this->service->index();        
    }

    public function store(SubunidadStoreRequest $request)
    {
        return $this->service->store($request);
    }
    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(SubunidadStoreRequest $request, $id)
    {
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }

}
