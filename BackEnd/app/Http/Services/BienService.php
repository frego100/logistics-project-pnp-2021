<?php

namespace App\Http\Services;

use App\Models\Bien;

class BienService 
{
    public function index(){
       return Bien::all();
    }
    
    public function store($request){
        $bien = new Bien();
        $bien->nombre = $request->nombre;
        $bien->save();

        return response()->json($bien, 200);
    }

    public function update($request, $id){
        $bien = Bien::findOrFail($id);
        $bien->nombre = $request->nombre;
        $bien->save();

        return $bien;
    }

    public function show($id)
    {
         $bien = Bien::findOrFail($id);
         return $bien;
    }

    public function destroy($id)
    {
        $bien = Bien::findOrFail($id);
        $bien->delete();
        return $bien;
    }

}