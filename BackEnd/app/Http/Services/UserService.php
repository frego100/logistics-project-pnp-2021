<?php

namespace App\Http\Services;

use App\Models\User;

class UserService
{
    public function index(){
       return User::all();
    }

    public function store($request){
        $user = new User();
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->dni = $request->dni;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json($user, 200);
    }

    public function update($request, $id){
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->apellido = $request->apellido;
        $user->dni = $request->dni;
        $user->email = $request->email;
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return $user;
    }

    public function show($id)
    {
         $user = User::findOrFail($id);
         return $user;
    }

    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }

}
