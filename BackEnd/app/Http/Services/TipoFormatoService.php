<?php

namespace App\Http\Services;

use App\Models\TipoFormato;

class TipoFormatoService 
{
    public function index(){
       return TipoFormato::all();
    }
    
    public function store($request){
        $tipo_formato = new TipoFormato();
        $tipo_formato->nombre = $request->nombre;
        $tipo_formato->save();

        return response()->json($tipo_formato, 200);
    }

    public function update($request, $id){
        $tipo_formato = TipoFormato::findOrFail($id);
        $tipo_formato->nombre = $request->nombre;
        $tipo_formato->save();

        return $tipo_formato;
    }

    public function show($id)
    {
         $tipo_formato = TipoFormato::findOrFail($id);
         return $tipo_formato;
    }

    public function destroy($id)
    {
        $tipo_formato = TipoFormato::findOrFail($id);
        $tipo_formato->delete();
        return $tipo_formato;
    }

}