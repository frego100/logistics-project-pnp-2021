<?php

namespace App\Http\Services;

use App\Exceptions\LoginException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginService 
{
    public function login($request){
    
       $credentials = [
           'email' => $request->email,
           'password'=>$request->password
       ];
       $duracionToken = 60 * 24; // 1 dia 
       return $this->token($credentials, $duracionToken );
     
    }
    
    private function token($credentials, $duracionMinutos)
    {          
       $myTTL = $duracionMinutos; 
       JWTAuth::factory()->setTTL($myTTL); 

        if (! $token = JWTAuth::attempt($credentials)) {

            throw new LoginException("Usuario o Password Incorrecto");
        } 
        return [
            'token' => $token,
            'token_type' => 'bearer'
        ];
    }

}