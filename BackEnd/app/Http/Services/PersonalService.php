<?php

namespace App\Http\Services;

use App\Models\Personal;

class PersonalService 
{
    public function index(){
       return Personal::all();
    }
    
    public function store($request){
        $personal = new Personal();
        $personal->grado = $request->grado;
        $personal->nombre_completo = $request->nombre_completo;
        $personal->cip = $request->cip;
        $personal->dni = $request->dni;
        $personal->save();

        return response()->json($personal, 200);
    }

    public function update($request, $id){
        $personal = Personal::findOrFail($id);
        $personal->grado = $request->grado;
        $personal->nombre_completo = $request->nombre_completo;
        $personal->cip = $request->cip;
        $personal->dni = $request->dni;
        $personal->save();

        return $personal;
    }

    public function show($id)
    {
         $personal = Personal::findOrFail($id);
         return $personal;
    }

    public function destroy($id)
    {
        $personal = Personal::findOrFail($id);
        $personal->delete();
        return $personal;
    }

}