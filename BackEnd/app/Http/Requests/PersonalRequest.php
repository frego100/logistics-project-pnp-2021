<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalRequest extends BaseFormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "grado" => "required",
            "nombre_completo" => "required",
            "cip" => "required",
            "dni" => "required",
        ];
    }
}
