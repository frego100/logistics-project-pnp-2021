<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subunidad extends Model
{
    use HasFactory;
    protected $table = "subunidades";
}

