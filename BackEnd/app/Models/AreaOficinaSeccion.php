<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AreaOficinaSeccion extends Model
{
    use HasFactory;
    protected $table = "area_oficina_secciones";


    public function subunidad(){
        return $this->belongsTo(Subunidad::class);
    }
}

