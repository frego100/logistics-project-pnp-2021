<?php

namespace Tests\Feature;

use App\Models\AreaOficinaSeccion;
use App\Models\Subunidad;

class AreaOficinaSeccionControllerTest extends TestCase
{
    
    private $areaOficinaSeccion;
    protected function setUp(): void
    {
        parent::setUp();
        $this->areaOficinaSeccion = $this->createAreaOficinaSeccion();
    }

    public function test_index()
    {
        $url = route('areaOficinaSecciones');
        $response = $this->get($url);
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->areaOficinaSeccion->id;
        $url = route('areaOficinaSeccionShow', ['id'=> $id]);
        $response = $this->get($url);
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->areaOficinaSeccion->id;
        $url = route('areaOficinaSeccionDestroy', ['id'=> $id]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(AreaOficinaSeccion::all()->count(), 0);
    }

    public function test_update(){
        $id = $this->areaOficinaSeccion->id;
        $data = $this->dtoUpdate();
        $url = route('areaOficinaSeccionUpdate', ['id'=> $id]);
        $response = $this->put($url, $data);
        $this->areaOficinaSeccion->refresh();
 
        $response->assertStatus(200);
        $this->assertEquals($data['nombre'], $this->areaOficinaSeccion->nombre);
    }
    
    public function test_create(){
        $data = $this->dtoCreate();
        $url = route('areaOficinaSeccionStore');
        $response = $this->post($url, $data);

        $response->assertStatus(200);
        $this->assertEquals(AreaOficinaSeccion::all()->count(), 2);
    }
  
    private function createAreaOficinaSeccion()
    {
        $subunidad = $this->createSubunidad();

        $areaOficinaSeccion = new AreaOficinaSeccion();
        $areaOficinaSeccion->nombre = "Frente Interno";
        $areaOficinaSeccion->subunidad_id = $subunidad->id; 
        $areaOficinaSeccion->save();
        
        return $areaOficinaSeccion;
    }
    
    private function createSubunidad()
    {
        $subunidad = new Subunidad();
        $subunidad->nombre = "Contra Inteligencia";
        $subunidad->save();
        
        return $subunidad;
    }

    private function dtoCreate(){
        $subunidad = $this->areaOficinaSeccion->subunidad;
        $subunidad_id = $subunidad->id;
        return ['nombre' => 'Social', 'subunidad_id'=>$subunidad_id];
    }

    private function dtoUpdate(){
        $data = $this->dtoCreate();
        $data['nombre'] = "Jefatura";

        return $data;
    }
    
    
}
