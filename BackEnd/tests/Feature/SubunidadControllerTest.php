<?php

namespace Tests\Feature;

use App\Models\Bien;
use App\Models\Subunidad;

class SubunidadControllerTest extends TestCase
{
    
    private $subunidad;
    protected function setUp(): void
    {
        parent::setUp();
        $this->subunidad = $this->createSubunidad();
    }

    public function test_index()
    {
        $url = route('subunidades');
        $response = $this->get($url);
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->subunidad->id;
        $url = route('subunidadShow', ['id'=> $id]);
        $response = $this->get($url);
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->subunidad->id;
        $url = route('subunidadDestroy', ['id'=> $id]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(Subunidad::all()->count(), 0);
    }

    public function test_update(){
        $id = $this->subunidad->id;
        $data = ['nombre' => 'Bien A'];
        $url = route('subunidadUpdate', ['id'=> $id]);
        $response = $this->put($url, $data);
        $this->subunidad->refresh();
 
        $response->assertStatus(200);
        $this->assertEquals($data['nombre'], $this->subunidad->nombre);
    }
    
    public function test_create(){
        $data = ['nombre' => 'Bien A'];
        $url = route('subunidadStore');
        $response = $this->post($url, $data);
        $response->assertStatus(200);
        $this->assertEquals(Subunidad::all()->count(), 2);
    }
  
    private function createSubunidad()
    {
        $subunidad = new Subunidad();
        $subunidad->nombre = "Contrainteligencia";
        $subunidad->save();
        
        return $subunidad;
    }
    
    
}
