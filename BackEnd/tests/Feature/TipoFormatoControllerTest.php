<?php

namespace Tests\Feature;

use App\Models\Bien;
use App\Models\Subunidad;
use App\Models\TipoFormato;

class TipoFormatoControllerTest extends TestCase
{
    
    private $tipoFormato;
    protected function setUp(): void
    {
        parent::setUp();
        $this->tipoFormato = $this->createFormato();
    }

    public function test_index()
    {
        $url = route('tipoFormato');
        $response = $this->get($url);
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->tipoFormato->id;
        $url = route('tipoFormatoShow', ['id'=> $id]);
        $response = $this->get($url);
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->tipoFormato->id;
        $url = route('tipoFormatoDestroy', ['id'=> $id]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(TipoFormato::all()->count(), 0);
    }

    public function test_update(){
        $id = $this->tipoFormato->id;
        $data = ['nombre' => 'Equipo Policial'];
        $url = route('tipoFormatoUpdate', ['id'=> $id]);
        $response = $this->put($url, $data);
        $this->tipoFormato->refresh();
 
        $response->assertStatus(200);
        $this->assertEquals($data['nombre'], $this->tipoFormato->nombre);
    }
    
    public function test_create(){
        $data = ['nombre' => 'Maquinarias, equipos, muebles y enseres'];
        $url = route('tipoFormatoStore');
        $response = $this->post($url, $data);
        $response->assertStatus(200);
        $this->assertEquals(TipoFormato::all()->count(), 2);
    }
  
    private function createFormato()
    {
        $tipoFormato = new TipoFormato();
        $tipoFormato->nombre = "Bienes Auxiliares";
        $tipoFormato->save();
        
        return $tipoFormato;
    }
    
    
}
