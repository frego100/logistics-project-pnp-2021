<?php

namespace Tests\Feature;

use App\Models\Bien;

class BienControllerTest extends TestCase
{
    
    private $bien;
    protected function setUp(): void
    {
        parent::setUp();
        $this->bien = $this->createBien();
    }

    public function test_index()
    {
        $url = route('bienes');
        $response = $this->get($url);
        $response->assertStatus(200);
    }
    
    public function test_show()
    {
        $id = $this->bien->id;
        $url = route('bienShow', ['id'=> $id]);
        $response = $this->get($url);
        $response->assertStatus(200);
    }

    public function test_destroy(){
        $id = $this->bien->id;
        $url = route('bienDestroy', ['id'=> $id]);
        $response = $this->delete($url);
        $response->assertStatus(200);
        $this->assertEquals(Bien::all()->count(), 0);
    }

    public function test_update(){
        $id = $this->bien->id;
        $data = ['nombre' => 'Bien A'];
        $url = route('bienUpdate', ['id'=> $id]);
        $response = $this->put($url, $data);
        $this->bien->refresh();
 
        $response->assertStatus(200);
        $this->assertEquals($data['nombre'], $this->bien->nombre);
    }
    
    public function test_create(){
        $data = ['nombre' => 'Bien A'];
        $url = route('bienStore');
        $response = $this->post($url, $data);

        $response->assertStatus(200);
        $this->assertEquals(Bien::all()->count(), 2);
    }
  
    private function createBien()
    {
        $bien = new Bien();
        $bien->nombre = "Bien";
        $bien->save();
        
        return $bien;
    }
    
    
}
