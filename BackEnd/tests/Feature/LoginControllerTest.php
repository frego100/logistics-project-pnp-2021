<?php

namespace Tests\Feature;

use App\Exceptions\LoginException;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Auth;

class LoginControllerTest extends TestCase
{    
    public function test_login_credenciales_correctas()
    {
        $credenciales = $this->credencialesValidas(); 
        $url = route('login');

        $response = $this->post($url, $credenciales);

        $response->assertStatus(200);
    }

    public function test_login_credenciales_incorrectas(){
        $credenciales = $this->credencialesInvalidas();
        $url = route('login');

        $response = $this->post($url, $credenciales);
        $response->assertStatus(400);
    }

    public function test_login_credenciales_incorrectas_generar_exception(){
        $this->withoutExceptionHandling();
        $credenciales = $this->credencialesInvalidas();
        $url = route('login');

        $this->expectException(LoginException::class);
        $this->post($url, $credenciales);
    }

    private function credencialesInvalidas(){
        return  [
            'email'=>"admin@admin.com",
            'password'=>"claveinvalida"
        ];
    }

    private function credencialesValidas(){
        return $this->credenciales();    
    }
     



}
