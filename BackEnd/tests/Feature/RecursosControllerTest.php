<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Auth;

class RecursosControllerTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function test_recurso()
    {
        $url = route('recurso');
        $response = $this->get($url, $this->headerToken());

        $response->assertStatus(200);
    }
    public function test_recurso_nuevo()
    {
        $url = route('recursoNuevo');
        $data = ['data' => "a"];
        $response = $this->post($url, $data, $this->headerToken());

        $response->assertStatus(200);
    }
}
