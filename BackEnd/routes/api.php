<?php

use App\Http\Controllers\AreaOficinaSeccionController;
use App\Http\Controllers\BienController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\TipoFormatoController;
use App\Http\Controllers\SubunidadController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::group(['middleware' => ['jwt.verify']], function () {

    Route::get('/recurso', function () {
        return "recurso";
    })->name('recurso');

    Route::post('/recurso', function () {
        return "recurso";
    })->name('recursoNuevo');
});

//BIENES
Route::get('/bienes', [BienController::class, 'index'])->name('bienes');
Route::get('/bienes/{id}', [BienController::class, 'show'])->name('bienShow');
Route::put('/bienes/{id}', [BienController::class, 'update'])->name('bienUpdate');
Route::post('/bienes', [BienController::class, 'store'])->name("bienStore");
Route::delete('/bienes/{id}', [BienController::class, 'destroy'])->name("bienDestroy");
Route::get('/download-pdf', [BienController::class,'downloadPDF']);

//PERSONAL
Route::get('/personal', [PersonalController::class, 'index'])->name('personal');
Route::get('/personal/{id}', [PersonalController::class, 'show'])->name('personalShow');
Route::put('/personal/{id}', [PersonalController::class, 'update'])->name('personalUpdate');
Route::post('/personal', [PersonalController::class, 'store'])->name("personalStore");
Route::delete('/personal/{id}', [PersonalController::class, 'destroy'])->name("personalDestroy");

//SUBUNIDADES
Route::get('/subunidades', [SubunidadController::class, 'index'])->name('subunidades');
Route::get('/subunidades/{id}', [SubunidadController::class, 'show'])->name('subunidadShow');
Route::put('/subunidades/{id}', [SubunidadController::class, 'update'])->name('subunidadUpdate');
Route::post('/subunidades', [SubunidadController::class, 'store'])->name("subunidadStore");
Route::delete('/subunidades/{id}', [SubunidadController::class, 'destroy'])->name("subunidadDestroy");

//AREA OFICINA SECCION
Route::get('/area-oficina-secciones', [AreaOficinaSeccionController::class, 'index'])->name('areaOficinaSecciones');
Route::get('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'show'])->name('areaOficinaSeccionShow');
Route::put('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'update'])->name('areaOficinaSeccionUpdate');
Route::post('/area-oficina-secciones', [AreaOficinaSeccionController::class, 'store'])->name("areaOficinaSeccionStore");
Route::delete('/area-oficina-secciones/{id}', [AreaOficinaSeccionController::class, 'destroy'])->name("areaOficinaSeccionDestroy");

//TIPO_FORMATO
Route::get('/tipo-formato', [TipoFormatoController::class, 'index'])->name('tipoFormato');
Route::get('/tipo-formato/{id}', [TipoFormatoController::class, 'show'])->name('tipoFormatoShow');
Route::put('/tipo-formato/{id}', [TipoFormatoController::class, 'update'])->name('tipoFormatoUpdate');
Route::post('/tipo-formato', [TipoFormatoController::class, 'store'])->name("tipoFormatoStore");
Route::delete('/tipo-formato/{id}', [TipoFormatoController::class, 'destroy'])->name("tipoFormatoDestroy");

//USUARIOS
Route::get('/usuarios', [UserController::class, 'index'])->name('usuario');
Route::get('/usuarios/{id}', [UserController::class, 'show'])->name('usuarioShow');
Route::put('/usuarios/{id}', [UserController::class, 'update'])->name('usuarioUpdate');
Route::post('/usuarios', [UserController::class, 'store'])->name("usuarioStore");
Route::delete('/usuarios/{id}', [UserController::class, 'destroy'])->name("usuarioDestroy");
