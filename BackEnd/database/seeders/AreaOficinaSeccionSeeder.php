<?php

namespace Database\Seeders;

use App\Models\AreaOficinaSeccion;
use App\Models\Subunidad;
use Illuminate\Database\Seeder;

class AreaOficinaSeccionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $idAdministracion = 1;
        $idContraInteligencia = 2;
        $idBusqueda = 3;

        $areaOficinaSeccion1 = new AreaOficinaSeccion();
        $areaOficinaSeccion1->nombre = "Seguridad Digital";
        $areaOficinaSeccion1->subunidad_id = $idAdministracion;
        $areaOficinaSeccion1->save();

        $areaOficinaSeccion2 = new AreaOficinaSeccion();
        $areaOficinaSeccion2->nombre = "Frente Interno";
        $areaOficinaSeccion2->subunidad_id = $idContraInteligencia;
        $areaOficinaSeccion2->save();
        
        $areaOficinaSeccion3 = new AreaOficinaSeccion();
        $areaOficinaSeccion3->nombre = "Social";
        $areaOficinaSeccion3->subunidad_id = $idBusqueda;
        $areaOficinaSeccion3->save();

        $areaOficinaSeccion4 = new AreaOficinaSeccion();
        $areaOficinaSeccion4->nombre = "Jefatura de Administración";
        $areaOficinaSeccion4->subunidad_id = $idAdministracion;
        $areaOficinaSeccion4->save();

        $areaOficinaSeccion5 = new AreaOficinaSeccion();
        $areaOficinaSeccion5->nombre = "Mesa de Partes";
        $areaOficinaSeccion5->subunidad_id = $idAdministracion;
        $areaOficinaSeccion5->save();
      
    }

}
