<?php

namespace Database\Seeders;

use App\Models\TipoFormato;
use Illuminate\Database\Seeder;

class TipoFormatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipoFormato1 = new TipoFormato();
        $tipoFormato1->nombre = "Bienes Auxiliares";
        $tipoFormato1->save();

        $tipoFormato2 = new TipoFormato();
        $tipoFormato2->nombre = "Maquinarias, equipos, muebles y enseres";
        $tipoFormato2->save();

        $tipoFormato3 = new TipoFormato();
        $tipoFormato3->nombre = "Equipo Policial";
        $tipoFormato3->save();
    }
}
