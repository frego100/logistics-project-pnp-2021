<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Administrador";
        $user->apellido = "Admin";
        $user->dni = "71769047";
        $user->email = "admin@admin.com";
        $user->password = bcrypt("administrador");
        $user->save();
    }

}
