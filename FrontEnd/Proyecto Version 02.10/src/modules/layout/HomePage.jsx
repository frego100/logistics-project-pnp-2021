import React from 'react'
import AuthLoginScreen from '../auth/pages/AuthLoginScreen'
import UserCrearPage from '../general/users/UserCrearPage'
import UserListPage from '../general/users/UserListPage'

const HomePage = () => {
    return (
        <div>
            <AuthLoginScreen/>
        </div>
    )
}

export default HomePage
