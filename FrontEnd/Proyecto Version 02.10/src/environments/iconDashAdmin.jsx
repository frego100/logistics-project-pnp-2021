import usuario from "../assets/icons/icons_48/user_48.svg";
import personal from "../assets/icons/icons_48/personal_48.svg";
import subUnidades from "../assets/icons/icons_48/subUnidades_48.svg";
import areaOficinaSeccion from "../assets/icons/icons_48/AreaOficinaSeccion_48.svg";
import historial from "../assets/icons/icons_48/outline-policy_48.svg";

import bienesAuxiliares from "../assets/icons/icons_48/support_48.svg";
import equipoPolicial from "../assets/icons/icons_48/equipo_policial_48.svg";
import unidadTransporte from "../assets/icons/icons_48/service_Police_48.svg";
import bienesInternaDestru from "../assets/icons/icons_48/equipment_48.svg";
import maquinariaYEquipos from "../assets/icons/icons_48/excavator_48.svg";



export default {
  usuario: usuario,
  personal: personal,
  subUnidades: subUnidades,
  areaOficinaSeccion: areaOficinaSeccion,
  historial: historial,
  personal: personal,
  bienesAuxiliares: bienesAuxiliares,
  equipoPolicial: equipoPolicial,
  unidadTransporte: unidadTransporte,
  bienesInternaDestru: bienesInternaDestru,
  maquinariaYEquipos: maquinariaYEquipos,
};
