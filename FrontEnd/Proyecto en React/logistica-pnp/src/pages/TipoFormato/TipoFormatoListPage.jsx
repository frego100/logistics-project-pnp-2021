import React, { useEffect, useState } from "react";

import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import {
  deleteTipoFormatoById,
  getTipoFormatos,
} from "../../services/tipoFormatoService";
const TipoFormatoListPage = () => {
  const [tipoFormato, setTipoFormatos] = useState([]);
  const [cargando, setCargando] = useState(true);

  const traerTipoFormato = () => {
    setCargando(true);
    getTipoFormatos().then((rpta) => {
      console.log(rpta);
      setTipoFormatos(rpta.data);
      setCargando(false);
    });
  };

  useEffect(() => {
    traerTipoFormato();
  }, []);

  const eliminarTipoFormato = (id) => {
    Swal.fire({
      title: "¿Seguro que deseas eliminar?",
      icon: "warning",
      text: "Los cambios serán irreversibles 😮",
      showCancelButton: true,
    }).then((rpta) => {
      if (rpta.isConfirmed) {
        //Aquí borro el usuario
        deleteTipoFormatoById(id).then((rpta) => {
          if (rpta.status === 200) {
            //Se comprueba que se eliminó correctamente
            traerTipoFormato(); //Se llama otra vez para setear la variable de estado y recargar la página automáticamente al borrar un usuario
          }
        });
      }
    });
  };

  return (
    <div>
      <main className="container">
        <div className="row mt-4">
          <div className="col">
            <Link to={"/tipoFormato/crear"} className="btn btn-primary mb-5">
              Crear Tipo Formato
            </Link>
            {cargando ? (
              <div className="alert alert-info">
                <h4>Cargando ...</h4>
                <div className="spinner-border text-info" role="status">
                  <span className="visually-hidden">Loading...</span>
                </div>
              </div>
            ) : (
              <table className="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Fecha de Creación</th>
                    <th>Última Actualización</th>
                  </tr>
                </thead>

                <tbody>
                  {tipoFormato.map((objTipoFormato, i) => {
                    return (
                      <tr key={objTipoFormato.id}>
                        <td>{objTipoFormato.id}</td>
                        <td>{objTipoFormato.nombre}</td>
                        <td>{objTipoFormato.created_at}</td>
                        <td>{objTipoFormato.updated_at}</td>
                        <td>
                          <button
                            className="btn btn-danger"
                            onClick={() => {
                              eliminarTipoFormato(objTipoFormato.id);
                            }}
                          >
                            Eliminar
                          </button>
                          <Link
                            to={`/tipoformato/editar/${objTipoFormato.id}`}
                            className="btn btn-warning"
                          >
                            Editar
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            )}
          </div>
        </div>
      </main>
    </div>
  );
};

export default TipoFormatoListPage;
