import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import { deletePersonalById, getPersonal } from '../../services/Personal'
const PersonalListPage = () => {

  
    const [personal, setPersonal] = useState([])
    const [cargando, setCargando] = useState(true)

    const traerUsuarios = () => {
        setCargando(true)
        getPersonal().then(rpta => {
            console.log(rpta)
            setPersonal(rpta.data)
            setCargando(false)
        })
    }

    useEffect(() => {
        traerUsuarios()
    }, [])


    const eliminarUsuario = id => {
        Swal.fire({
            title: '¿Seguro que deseas eliminar?',
            icon: 'warning',
            text: 'Los cambios serán irreversibles 😮',
            showCancelButton: true
        }).then((rpta) => {
            if (rpta.isConfirmed) {
                //Aquí borro el usuario
                deletePersonalById(id).then((rpta) => {
                    if (rpta.status === 200) {
                        //Se comprueba que se eliminó correctamente
                        traerUsuarios() //Se llama otra vez para setear la variable de estado y recargar la página automáticamente al borrar un usuario
                    }
                })
            }
        })
    }

    return (
     
        <div>
            <main className="container">
                <div className="row mt-4">
                    <div className="col">
                        <Link to={"/personal/crear"} className="btn btn-primary mb-5">Crear Personal</Link>
                        {
                            cargando ? <div className="alert alert-info">
                                <h4>Cargando ...</h4>
                                <div className="spinner-border text-info" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div> :
                                <table className="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Grado</th>
                                            <th>Nombres completos</th>
                                            <th>CIP</th>
                                            <th>DNI</th>
                                            <th>Fecha de Creación</th>
                                            <th>Última Actualización</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {
                                            personal.map((objPersonal, i) => {
                                                return (
                                                    <tr key={objPersonal.id}>     
                                                        <td>{objPersonal.id}</td>
                                                        <td>{objPersonal.grado}</td>
                                                        <td>{objPersonal.nombre_completo}</td>
                                                        <td>{objPersonal.cip}</td>
                                                        <td>{objPersonal.dni}</td>
                                                        <td>{objPersonal.created_at}</td>
                                                        <td>{objPersonal.updated_at}</td>
                                                        <td>
                                                            <button
                                                                className="btn btn-danger"
                                                                onClick={() => {
                                                                    eliminarUsuario(objPersonal.id);
                                                                }}
                                                            >
                                                                Eliminar
                                                            </button>
                                                            <Link to={`/personal/editar/${objPersonal.id}`}
                                                                className="btn btn-warning"
                                                            >
                                                                Editar
                                                            </Link>
                                                        </td>
                                                    </tr>

                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                        }

                    </div>
                </div>
            </main>
        </div>
    )
}

export default PersonalListPage
