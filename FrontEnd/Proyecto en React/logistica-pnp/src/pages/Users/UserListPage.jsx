import React, { useEffect, useState } from 'react'

import { Link } from 'react-router-dom'
import Swal from 'sweetalert2'
import { deleteUsuarioById, getUsuarios } from '../../services/usuarioService'
const UserListPage = () => {

  
    const [usuarios, setUsuarios] = useState([])
    const [cargando, setCargando] = useState(true)

    const traerUsuarios = () => {
        setCargando(true)
        getUsuarios().then(rpta => {
            console.log(rpta)
            setUsuarios(rpta.data)
            setCargando(false)
        })
    }

    useEffect(() => {
        traerUsuarios()
    }, [])


    const eliminarUsuario = id => {
        Swal.fire({
            title: '¿Seguro que deseas eliminar?',
            icon: 'warning',
            text: 'Los cambios serán irreversibles 😮',
            showCancelButton: true
        }).then((rpta) => {
            if (rpta.isConfirmed) {
                //Aquí borro el usuario
                deleteUsuarioById(id).then((rpta) => {
                    if (rpta.status === 200) {
                        //Se comprueba que se eliminó correctamente
                        traerUsuarios() //Se llama otra vez para setear la variable de estado y recargar la página automáticamente al borrar un usuario
                    }
                })
            }
        })
    }

    return (
     
        <div>
            <main className="container">
                <div className="row mt-4">
                    <div className="col">
                        <Link to={"/usuarios/crear"} className="btn btn-primary mb-5">Crear Usuario</Link>
                        {
                            cargando ? <div className="alert alert-info">
                                <h4>Cargando ...</h4>
                                <div className="spinner-border text-info" role="status">
                                    <span className="visually-hidden">Loading...</span>
                                </div>
                            </div> :
                                <table className="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>DNI</th>
                                            <th>Email</th>
                                            <th>Fecha de Creación</th>
                                            <th>Última Actualización</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        {
                                            usuarios.map((objUsuario, i) => {
                                                return (
                                                    <tr key={objUsuario.id}>     
                                                        <td>{objUsuario.id}</td>
                                                        <td>{objUsuario.name}</td>
                                                        <td>{objUsuario.apellido}</td>
                                                        <td>{objUsuario.dni}</td>
                                                        <td>{objUsuario.email}</td>
                                                        <td>{objUsuario.created_at}</td>
                                                        <td>{objUsuario.updated_at}</td>
                                                        <td>
                                                            <button
                                                                className="btn btn-danger"
                                                                onClick={() => {
                                                                    eliminarUsuario(objUsuario.id);
                                                                }}
                                                            >
                                                                Eliminar
                                                            </button>
                                                            <Link to={`/usuarios/editar/${objUsuario.id}`}
                                                                className="btn btn-warning"
                                                            >
                                                                Editar
                                                            </Link>
                                                        </td>
                                                    </tr>

                                                )
                                            })
                                        }
                                    </tbody>
                                </table>
                        }

                    </div>
                </div>
            </main>
        </div>
    )
}

export default UserListPage
