//https://gitlab.com/frego100/logistics-project-pnp-2021/-/issues/61

import axios from "axios";

import { URL_BACKEND } from "../environments/environments";

export const getTipoFormatos = async () => {
  const rpta = await axios.get(`${URL_BACKEND}/tipo-formato`);
  return rpta;
};

export const deleteTipoFormatoById = async (id) => {
  const rpta = await axios.delete(`${URL_BACKEND}/tipo-formato/${id}`);
  return rpta;
};

export const postTipoFormato = async (objTipoFormato) => {
  const rpta = await axios.post(
    `${URL_BACKEND}/tipo-formato`,
    JSON.stringify(objTipoFormato),
    { headers: { "Content-Type": "application/json" } }
  );
  return rpta;
};

export const getTipoFormatosById = async (id) => {
  const rpta = await axios.get(`${URL_BACKEND}/tipo-formato/${id}`);
  return rpta;
};

export const putTipoFormatoById = async (objTipoFormato) => {
  const rpta = await axios.put(
    `${URL_BACKEND}/tipo-formato/${objTipoFormato.id}`,
    JSON.stringify(objTipoFormato),
    { headers: { "Content-Type": "application/json" } }
  );
  return rpta;
};
