import React from 'react'
import { Link } from "react-router-dom";

const Header = () => {
    return (
        <header>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <div className="container-fluid">
            <Link className="navbar-brand" href="/#">
              Logística PNP
            </Link>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link to="/" className="nav-link">
                    Home
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/usuarios" className="nav-link">
                    Usuarios
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/tipoformato" className="nav-link">
                    tipo formato
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    )
}

export default Header
